@api
Feature: I can call rest apis

  Scenario: Check comments from a post are fetched
    Given I call the api to get the comments from a post
    Then I should get the comments returned

  Scenario: check if employee getting created
    Given I call the api to post the employee
    Then I should get the employee
