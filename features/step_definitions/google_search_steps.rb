Given /^I am on (.+)$/ do |url|
	@browser.goto "http://www.google.com/ncr"
end

When /^I fill in "([^"]*)" found by "([^"]*)" with "([^"]*)"$/ do |value, type, keys|
	puts keys
  @browser.text_field(name: "q").send_keys keys
end

When /^I submit$/ do
	@browser.text_field(name: 'q').send_keys(:return)
	sleep 1
end

Then /^I should see title "([^"]*)"$/ do |title|
	expect(@browser.title).to eq title
end
